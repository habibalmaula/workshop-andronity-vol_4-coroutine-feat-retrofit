package com.habibalmaula.andronitycoroutineretrofit

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.habibalmaula.andronitycoroutineretrofit.entity.Movie
import com.habibalmaula.andronitycoroutineretrofit.network.UtilsApi
import kotlinx.android.synthetic.main.layout_item.view.*
import kotlinx.coroutines.withContext

class MovieAdapter(private val movieList: List<Movie>) : RecyclerView.Adapter<MovieAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieAdapter.ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.layout_item, parent, false))

    }

    override fun getItemCount(): Int  = movieList.size

    override fun onBindViewHolder(holder: MovieAdapter.ViewHolder, position: Int) {
        holder.bind(movieList[position])
    }


    class ViewHolder(private val containerView : View) : RecyclerView.ViewHolder(containerView) {

        fun bind(movie : Movie){
            itemView.apply {
                tv_title.text = movie.title
                tv_overview.text = movie.overview

                Glide.with(context)
                    .load(UtilsApi.BASE_POSTER_URL+ movie.poster_path)
                    .into(iv_poster)

            }





        }

    }
}