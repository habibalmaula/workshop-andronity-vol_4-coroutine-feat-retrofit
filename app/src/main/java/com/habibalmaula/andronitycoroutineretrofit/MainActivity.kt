package com.habibalmaula.andronitycoroutineretrofit

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.habibalmaula.andronitycoroutineretrofit.utils.ProgressLoading
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val mainViewModel : MainViewModel by lazy {
        ViewModelProvider(this).get(MainViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        mainViewModel.getContentMovie()

        mainViewModel.listMovie.observe(this, Observer {
            if (it.isEmpty()){
                rv_movie.visibility = View.GONE
                layout_status.visibility = View.VISIBLE

                iv_status.setImageResource(R.drawable.ic_no_data)
                tv_status.text = "Yahh... datanya gak ada!"

            }else{
                rv_movie.visibility = View.VISIBLE
                layout_status.visibility = View.GONE

                val adapter = MovieAdapter(it)
                rv_movie.adapter = adapter
                adapter.notifyDataSetChanged()


            }

        })


        mainViewModel.progress.observe(this, Observer {
            when(it){
                ProgressLoading.LOADING->{
                    rv_movie.visibility = View.GONE
                    layout_status.visibility = View.GONE
                    shimmer_loading.visibility = View.VISIBLE

                }
                ProgressLoading.DONE->{
                    shimmer_loading.visibility = View.GONE

                }
                ProgressLoading.ERROR->{
                    rv_movie.visibility = View.GONE
                    shimmer_loading.visibility = View.GONE
                    layout_status.visibility = View.VISIBLE

                    iv_status.setImageResource(R.drawable.ic_connection_error)
                    tv_status.text = "Yahh...  internetnya mati!"

                }
            }
        })



        interaction()





    }

    private fun interaction() {
        //refreshing
        swipe_refresh.setOnRefreshListener {
            swipe_refresh.isRefreshing = false

            val textSearch = et_search.text.toString()

            if (textSearch!=""){
                mainViewModel.searchMovie(textSearch)

            }else{
                mainViewModel.getContentMovie()

            }


        }

        //searching
        et_search.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(s: Editable?) {
                if (s.toString()!=""){
                    Handler().postDelayed(
                        {
                            mainViewModel.searchMovie("$s")
                        }, 1500)


                }else{
                    mainViewModel.getContentMovie()

                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {




            }


        })

    }
}
