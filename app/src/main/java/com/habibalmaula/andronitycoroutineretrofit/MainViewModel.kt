package com.habibalmaula.andronitycoroutineretrofit

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.habibalmaula.andronitycoroutineretrofit.entity.Movie
import com.habibalmaula.andronitycoroutineretrofit.network.UtilsApi
import com.habibalmaula.andronitycoroutineretrofit.utils.ProgressLoading
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch

class MainViewModel : ViewModel() {


    private val mMovie = MutableLiveData<List<Movie>>()
    private val mProgress = MutableLiveData<ProgressLoading>()

    val listMovie : LiveData<List<Movie>>
        get() = mMovie

    val progress : LiveData<ProgressLoading>
        get() = mProgress



    fun getContentMovie(){
        mProgress.value = ProgressLoading.LOADING
        viewModelScope.launch {
            try {
                val response = UtilsApi().getDataMovie()
                mMovie.value = response
                mProgress.value = ProgressLoading.DONE
            }catch (e: Throwable){
                mProgress.value = ProgressLoading.ERROR
                e.printStackTrace()
            }
        }
    }

    fun searchMovie(query : String){
        mProgress.value = ProgressLoading.LOADING
        viewModelScope.launch {
            try {
                val response = UtilsApi().getSearchMovie(query)
                mMovie.value = response
                mProgress.value = ProgressLoading.DONE
            }catch (e: Throwable){
                mProgress.value = ProgressLoading.ERROR
                e.printStackTrace()
            }
        }
    }


    override fun onCleared() {
        super.onCleared()
        viewModelScope.cancel()
    }


}